**GCMeasure**

Commands:

sm_gcmeasure - opens the measure menu

Features:

* A 32x32 hull measuring mode.
* Normal point measuring.
* Fast gap measuring.


**Pictures:**

![picture](https://cdn.discordapp.com/attachments/495343503870656513/635808599154884623/unknown.png)
![picture](https://cdn.discordapp.com/attachments/495343503870656513/635808588518129664/unknown.png)
![picture](https://cdn.discordapp.com/attachments/495343503870656513/635808612396040195/unknown.png)